
package co.edu.udistrital.jmpinedat.Catastro;

/**
 *
 * @author JUAN MANUEL PINEDA 
 * @version 1.0
 */
public class Predio {
    
    
    //atributo
    protected String cedulaCatastral;

    
    //metodo 1
    public String getCedulaCatastral() {
        return cedulaCatastral;
    }
    
    //para verificar la consistencia de los datos de entrada
    //metodo 2
    public void setCedulaCatastral(String cedulaCatastral) {
        this.cedulaCatastral = cedulaCatastral;
    }
    
    //metodo constructor
   public Predio(String cedulaCatastral) {    
        this.cedulaCatastral = cedulaCatastral;
    }
   
   public Predio() {    
        
    }
   
   //Sobrecarga de métodos 
   public double calcularImpuesto(int indice,boolean exencion){
       
       return 5000000*indice-0.2*5000000;
       
   }
   
}
